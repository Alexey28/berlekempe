﻿namespace Berlekempe_algorithm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pCompoBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.startTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.logText = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pCompoBox
            // 
            this.pCompoBox.FormattingEnabled = true;
            this.pCompoBox.Items.AddRange(new object[] {
            "2",
            "3",
            "5",
            "7",
            "11",
            "13"});
            this.pCompoBox.Location = new System.Drawing.Point(12, 25);
            this.pCompoBox.Name = "pCompoBox";
            this.pCompoBox.Size = new System.Drawing.Size(98, 21);
            this.pCompoBox.TabIndex = 0;
            this.pCompoBox.SelectedIndexChanged += new System.EventHandler(this.pCompoBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите параметр p:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(284, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Введите исходную последовательность в одну строку:";
            // 
            // startTextBox
            // 
            this.startTextBox.Location = new System.Drawing.Point(12, 77);
            this.startTextBox.Name = "startTextBox";
            this.startTextBox.Size = new System.Drawing.Size(537, 20);
            this.startTextBox.TabIndex = 3;
            this.startTextBox.Text = "00010443432210141121011003";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Вычислить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = " Искомый полином для ГВП:";
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new System.Drawing.Point(12, 151);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(184, 13);
            this.resultLabel.TabIndex = 6;
            this.resultLabel.Text = "Здесь будет отображён результат.";
            // 
            // logText
            // 
            this.logText.Location = new System.Drawing.Point(11, 195);
            this.logText.Name = "logText";
            this.logText.Size = new System.Drawing.Size(538, 118);
            this.logText.TabIndex = 7;
            this.logText.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Проверочный полином для ГВП:";
            // 
            // checkResult
            // 
            this.checkResult.AutoSize = true;
            this.checkResult.Location = new System.Drawing.Point(12, 179);
            this.checkResult.Name = "checkResult";
            this.checkResult.Size = new System.Drawing.Size(184, 13);
            this.checkResult.TabIndex = 9;
            this.checkResult.Text = "Здесь будет отображён результат.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 325);
            this.Controls.Add(this.checkResult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.logText);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.startTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pCompoBox);
            this.Name = "Form1";
            this.Text = "Алгоритм Берлекемпа";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox pCompoBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox startTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.RichTextBox logText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label checkResult;
    }
}

